set nocompatible
filetype off            " for vundle

if has("vim_starting")
set rtp+=$VIM/bundle/neobundle.vim
call neobundle#begin(expand('$VIM/bundle'))
NeoBundleFetch 'Shougo/neobundle.vim'
call neobundle#end()
endif

filetype plugin indent on     " required!
filetype indent on
syntax on


" NeoBundle
" :NeoBundleInstall/NeoBundleUpdate/NeoBundleClean
NeoBundle 'Shougo/neobundle.vim'
NeoBundle 'Shougo/vimproc.vim'
NeoBundle 'Shougo/vimshell'
NeoBundle 'Shougo/unite.vim'
"Uniteで最近使ったファイルを表示
NeoBundle 'Shougo/neomru.vim'

"Unite用source 
":Unite colorscheme -auto-preview
NeoBundle 'ujihisa/unite-colorscheme'


"neocomplete or neocomplecache を環境に応じて
NeoBundle 'Shougo/neocomplete.vim'
"neocomplete設定
" 起動時に有効化
let g:neocomplete#enable_at_startup = 1
" 補完をキャンセルしてカーソル移動(ここらへんはご自由に！)
inoremap <expr><C-h> neocomplete#cancel_popup() . "\<left>"
inoremap <expr><C-l> neocomplete#cancel_popup() . "\<right>"


"ファイルの構文エラーをチェックしてくれる 
NeoBundle 'scrooloose/syntastic'
":VimFiler ファイルやディレクトリをわかりやすく表示してくれる
NeoBundle 'Shougo/vimfiler.vim'
"コマンドラインの上の行にわかりやすく情報表示
NeoBundle 'itchyny/lightline.vim'

"lightline設定
let g:lightline = {
        \ 'mode_map': {'c': 'NORMAL'},
        \ 'active': {
        \   'left': [ [ 'mode', 'paste' ], [ 'fugitive', 'filename' ] ]
        \ },
        \ 'component_function': {
        \   'modified': 'MyModified',
        \   'readonly': 'MyReadonly',
        \   'fugitive': 'MyFugitive',
        \   'filename': 'MyFilename',
        \   'fileformat': 'MyFileformat',
        \   'filetype': 'MyFiletype',
        \   'fileencoding': 'MyFileencoding',
        \   'mode': 'MyMode'
        \ }
        \ }

function! MyModified()
  return &ft =~ 'help\|vimfiler\|gundo' ? '' : &modified ? '+' : &modifiable ? '' : '-'
endfunction

function! MyReadonly()
  return &ft !~? 'help\|vimfiler\|gundo' && &readonly ? 'x' : ''
endfunction

function! MyFilename()
  return ('' != MyReadonly() ? MyReadonly() . ' ' : '') .
        \ (&ft == 'vimfiler' ? vimfiler#get_status_string() :
        \  &ft == 'unite' ? unite#get_status_string() :
        \  &ft == 'vimshell' ? vimshell#get_status_string() :
        \ '' != expand('%:t') ? expand('%:t') : '[No Name]') .
        \ ('' != MyModified() ? ' ' . MyModified() : '')
endfunction

function! MyFugitive()
  try
    if &ft !~? 'vimfiler\|gundo' && exists('*fugitive#head')
      return fugitive#head()
    endif
  catch
  endtry
  return ''
endfunction

function! MyFileformat()
  return winwidth(0) > 70 ? &fileformat : ''
endfunction

function! MyFiletype()
  return winwidth(0) > 70 ? (strlen(&filetype) ? &filetype : 'no ft') : ''
endfunction

function! MyFileencoding()
  return winwidth(0) > 70 ? (strlen(&fenc) ? &fenc : &enc) : ''
endfunction

function! MyMode()
  return winwidth(0) > 60 ? lightline#mode() : ''
endfunction



":NERDTree ディレクトリ，ファイルなどのツリーを表示
NeoBundle 'scrooloose/nerdtree'
"Ctrl+p でカレントディレクトリのファイルを表示 文字入力で絞込み
NeoBundle 'kien/ctrlp.vim'

"colorschemeをインストール
" solarized カラースキーム
NeoBundle 'altercation/vim-colors-solarized'
" mustang カラースキーム
NeoBundle 'croaker/mustang-vim'
" wombat カラースキーム
NeoBundle 'jeffreyiacono/vim-colors-wombat'
" jellybeans カラースキーム
NeoBundle 'nanotech/jellybeans.vim'
" lucius カラースキーム
NeoBundle 'vim-scripts/Lucius'
" zenburn カラースキーム
NeoBundle 'vim-scripts/Zenburn'
" mrkn256 カラースキーム
NeoBundle 'mrkn/mrkn256.vim'
" railscasts カラースキーム
NeoBundle 'jpo/vim-railscasts-theme'
" pyte カラースキーム
NeoBundle 'therubymug/vim-pyte'
" molokai カラースキーム
NeoBundle 'tomasr/molokai'

"  -----------------------------------------------
"  MyOriginalSetting

set background=dark
set fileencodings=iso-2022-jp,utf-8,euc-jp,ucs-2le,usc-2,cp932
set cursorline "カーソル行の背景色を変える
set directory=C:/temp "スワップファイルをtempへ
set backupdir=C:/temp "バックアップファイルをtempへ
set noundofile "undoファイルを作らない
"set shell=C:/Console2/Console.exe "ShellをConsole2へ
"左右のカーソル移動で行間移動可能にする。
set nocompatible
set whichwrap=b,s,h,l,<,>,[,]
":makeコマンドでnmakeを設定
"set makeprg=nmake
inoremap <silent> <C-e> <Esc>
vnoremap <silent> <C-e> <Esc>
